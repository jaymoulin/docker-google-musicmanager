![logo](logo.png)

Google Music Manager - Docker Image (Multiarch)
===============================================

[![Docker Pulls](https://img.shields.io/docker/pulls/jaymoulin/google-musicmanager.svg)](https://hub.docker.com/r/jaymoulin/google-musicmanager/)
[![Docker stars](https://img.shields.io/docker/stars/jaymoulin/google-musicmanager.svg)](https://hub.docker.com/r/jaymoulin/google-musicmanager/)
[![PayPal donation](https://github.com/jaymoulin/jaymoulin.github.io/raw/master/ppl.png "PayPal donation")](https://www.paypal.me/jaymoulin)
[![Buy me a coffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png "Buy me a coffee")](https://www.buymeacoffee.com/3Yu8ajd7W)
[![Become a Patron](https://badgen.net/badge/become/a%20patron/F96854 "Become a Patron")](https://patreon.com/jaymoulin)

(This product is available under a free and permissive license, but needs financial support to sustain its continued improvements. In addition to maintenance and stability there are many desirable features yet to be added.)

THIS REPOSITORY IS AUTO-UPDATED BY [GITHUB-RELEASE-NOTIFIER](https://github.com/femtopixel/github-release-notifier) (https://github.com/femtopixel/github-release-notifier)

This image allows you to download and upload your Google Music Library to/from a selected folder.
This image is based on [Google MusicManager](https://github.com/jaymoulin/google-music-manager)

Before using this image and `UPLOAD` / `DOWNLOAD` parameter, please be aware that specific uploader/download docker images exists on my repo. 

Installation
---

```
docker run -d --restart=always -v /path/to/your/upload/library:/media/library/upload -v /path/to/your/download/library:/media/library/download --name googlemusicmanager jaymoulin/google-musicmanager
```

You must define your path to your upload library in a volume to `/media/library/upload`
You must define your path to your download library in a volume to `/media/library/download`

You can also mount a folder to `/root/oauth` to keep or reuse your key

See environment variables to tweak some behaviour

Environment variables
---------------------

These environment variable will produce a different behaviour

* `REMOVE` : Remove file on successful upload (boolean, (default: false)) - pass to true if you want to delete files
* `ONESHOT` : Execute only once without listening to folder events (boolean, (default: false)) - pass to true if you want to execute only once (also remove `--restart=always` from docker parameters)  
* `UPLOADER_ID` : Identity of your device, must be your MAC address in uppercase 
    (default: false, which means your actual MAC address) - Change this value only if you know what you are doing and had `MAX_PER_MACHINE_USERS_EXCEEDED` error
* `DOWNLOAD` : Enable/Disable downloading from Google Music (boolean, (default: true)) - pass false if you do not want to download files
* `UPLOAD` : Enable/Disable uploading from Google Music (boolean, (default: true)) - pass false if you do not want to upload files
* `DEDUP_API` : Url to the deduplicate API (string (default: None)) - Will call deduplicate API before trying to sample and upload to Google Music
* `LOGIN` : Login (for cover art uploading) (string (default: None)) - Login of your Google Music account for cover art uploading
* `PASSWORD` : Password (for cover art uploading) (string (default: None)) - Password of your Google Music account for cover art uploading

### Example

```
docker run -d --restart=always -v /path/to/your/upload/library:/media/library/upload -v /path/to/your/download/library:/media/library/download -e REMOVE=true --name googlemusicmanager jaymoulin/google-musicmanager
```

will delete files after successful upload

Deduplicate
-----------

You can (un)mark files as duplicate thanks to the deduplicate API included.
For example, if you already know all your library was already uploaded to Google Music, you can mark all files as already uploaded in the deduplicate api.

```
docker exec googlemusicmanager google-music-upload-deduplicate --deduplicate_api http://172.17.0.1 -d /media/library/upload
```

Consult [Google Music Manager Uploader Deduplicate](https://github.com/jaymoulin/google-music-manager-uploader#deduplicate) for further informations.

Configuration
---
First, you have to allow the container to access your Google Music account
```
docker exec -ti googlemusicmanager auth
```
Then follow prompted instructions.

You will be asked to go to a Google URL to allow the connection:

```
Visit the following url:
 https://accounts.google.com/o/oauth2/v2/auth?client_id=XXXXXXXXXXX.apps.googleusercontent.com&access_type=offline&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fmusicmanager&response_type=code&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob
Follow the prompts, then paste the auth code here and hit enter:
```

Once done, restart the container to start downloading your library
```
docker restart googlemusicmanager
```

Appendixes
---

### Install Docker

If you don't have Docker installed yet, you can do it easily in one line using this command
 
```
curl -sSL "https://gist.githubusercontent.com/jaymoulin/e749a189511cd965f45919f2f99e45f3/raw/0e650b38fde684c4ac534b254099d6d5543375f1/ARM%2520(Raspberry%2520PI)%2520Docker%2520Install" | sudo sh && sudo usermod -aG docker $USER
```
